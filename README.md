Thank you for your interest in working at Bananatag!

As a part of your interview process, we would like you to
solve this problem to showcase your knowledge of JavaScript
and Node.js.

You have 2 hours to complete this assignment and send us
your solution. The assignment has two parts. Please start
with Part_1.txt before moving on to Part_2.txt, and
implement your solution in the main.js file provided. If
you need to make any assumptions, please state them clearly
in the file as well.

Thank you again, and good luck!
